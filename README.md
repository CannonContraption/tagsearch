# Tagsearch
Search for files by a list of tags.

## Why?
Do you have photos? Do you think Shotwell/Digikam is a little too heavy
but you still want to be able to tag and search photos? This was the original
intent of tagsearch. While this purpose is the primary goal of the project,
tagsearch can be used also for many other things, essentially covering
any use case where out-of-file tagging might be useful. At present, it
only has built-in integration for sxiv, however since it is a very simple
shell script at heart, it should not be difficult to add other integrations
as needed.

## How?
Make a file called 'tags' in a directory. Every file in that directory can then be tagged using the following format:


    filename.jpg tag1 tag2 tag3
    filename2.jpg draft tag2
    filename3.jpg photo scenery tag3
    
This will tag filename.jpg with the tags tag1 tag2 and tag3. filename2.jpg
will use draft and tag2, and filename3.jpg will have the tags photo scenery
and tag3. Searching using tagsearch in any parent directory to the one
containing these files (or in the directory itself) for the tag tag2 will
list out the files filename2.jpg and filename3.jpg. If invoked with the photo
option, tagsearch will automatically pipe these filenames into sxiv for quick
viewing.

## Usage
Tagsearch has a number of options that can be used.

    $ tagsearch help
                      __   __,  , _   __  ,
    _|_  _,   _,  () / () /  | /|/ \ / ()/|  |
     |  / |  / |  /\ >-  |   |  |__/|     |--|
     |_/\/|_/\/|//(_)\___/\_/\_/| \_/\___/|  |)
              (|         2021 CANNONCONTRAPTION
    
    Search through files using index files titled 'tags'.
    
    Step 1 is to create a file called 'tags' in every directory you want to have
    tags listed for. Echo a directory list into this file. Add a space, and every
    word after that becomes a tag. (NOTE: only works where no spaces exist in the
    full path to that file) This tool is used to search those files.
    
    Always lists/handles tags from the current directory, descending into all sub-
    directories.
    
    Options are:
    find     - Output a list of all matched file names
    photo    - Pipe the list of matched files into sxiv
    list     - list all potential tags (all tags in use)
    help     - this screen
    $
    
Tagsearch will display this help page whenever the help command is used,
or whenever an invalid command is used. It does use ANSI escape sequences,
so if you're not viewing this on Git{lab,hub}, then the screen will use
colors.
